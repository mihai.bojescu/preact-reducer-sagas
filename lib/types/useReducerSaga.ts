export type ReducerSaga<State, Actions, AllActions> = (saga: {
  state: State;
  action: Actions;
  dispatch: (action: AllActions) => void;
}) => void;

export type ReducerWithSaga<State, Actions> = {
  state: State;
  dispatch: (action: Actions) => void;
}
