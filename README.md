# Preact Reducer Sagas

A simple way to use sagas with a React useReducer hook.

## Motivation

I wanted to build a stack for moving away complex logic from the components to a business logic layer, and this helped me achieve this goal. I decided to extract this functionality so that other developers will benefit from it too.

## Usage examples

This project provides the following examples:

Name       | Description                                                                                                                           | Use-case
-----------|---------------------------------------------------------------------------------------------------------------------------------------|----------------------
`Example 1`| A simple example with one main reducer and one main saga                                                                              | A small TODO list app
`Example 2`| A more advanced example, with one main reducer and one main saga, that interacts with 2 specialised reducers, each with it's own saga.| A bigger application

These examples can be seen in [`./examples`](./examples). 