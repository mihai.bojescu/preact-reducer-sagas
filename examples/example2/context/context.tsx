import { createContext, FunctionalComponent } from 'preact'
import { ReducerWithSaga, useReducerWithSaga } from '../../../lib'
import { Actions, initialState, reducer, saga, State } from '../reducer'

export const Context = createContext<ReducerWithSaga<State, Actions>>({
  state: initialState,
  dispatch: () => undefined
})

export const Store: FunctionalComponent = ({ children }) => {
  const [state, dispatch] = useReducerWithSaga(reducer, initialState, saga)

  return (
    <Context.Provider value={{ state, dispatch }} children={children} />
  )
}
