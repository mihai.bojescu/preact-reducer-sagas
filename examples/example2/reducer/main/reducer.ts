import { Reducer } from 'preact/hooks'
import * as number from '../number'
import * as uuid from '../uuid'
import { Actions, State } from './types'

export const initialState: State = {
  Number: number.initialState,
  UUID: uuid.initialState
}

export const reducer: Reducer<State, Actions> = (state, action) => ({
  Number: number.reducer(state.Number, action),
  UUID: uuid.reducer(state.UUID, action)
})
