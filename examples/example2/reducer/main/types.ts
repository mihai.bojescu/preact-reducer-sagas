import * as number from '../number'
import * as uuid from '../uuid'

export type State = {
  Number: number.State;
  UUID: uuid.State;
};

export type Actions =
  | number.Actions
  | uuid.Actions
