import { Reducer } from 'preact/hooks'
import { Actions, State } from './types'

export const initialState: State = {
  data: null,
  isLoaded: false,
  error: null
}

export const reducer: Reducer<State, Actions> = (state, action) => {
  switch (action.type) {
    case 'FETCH_SUCCESSFUL':
      return {
        ...state,
        data: action.payload
      }
    case 'FETCH_FAILED':
      return {
        ...state,
        error: action.payload
      }
    default:
      return state
  }
}
