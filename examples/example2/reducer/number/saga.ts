
import { ReducerSaga } from '../../../../lib'
import { Actions, State } from './types'

export const saga: ReducerSaga<State, Actions, Actions> = ({ state, action, dispatch }) => {
  switch (action.type) {
    case 'FETCH':
      return fetchData({ state, action, dispatch })
    default:
      return null
  }
}

const fetchData: ReducerSaga<State, Actions, Actions> = async ({ dispatch }) => {
  try {
    const request = await fetch('http://www.randomnumberapi.com/api/v1.0/random?min=1&max=10&count=1')
    const json = await request.json()
    const data = json[0]

    dispatch({ type: 'FETCH_SUCCESSFUL', payload: data })
  } catch (err) {
    dispatch({ type: 'FETCH_FAILED', payload: err })
  }
}
